<?php
  session_start();
  include('views/layout/header.php');
  if(isset($_SESSION['error_login'])){echo $_SESSION['error_login'];}
?>
    <div class="container">      
      <div class="row">
        <div class="col-md-4 col-md-offset-4 col-xs-8">
            <img src="images/logo-trimaranweb1.jpg" class="logo img-responsive center-block" alt="Logo Ressourcerie">
            <form method="post" action="controller/login/controller_login.php" name="login" class="form-horizontal">
              <div class="form-group">
                <input name="user" placeholder="Identifiant" class="form-control" type="text" id="UserUsername"/>
              </div> 
              <div class="form-group">
                <input name="password" placeholder="Mot de passe" class="form-control" type="password" id="UserPassword"/>
              </div> 
              <div class="form-group">
                <button class="btn btn-success btn center-block bouton-connexion" type="submit" value="Connexion"><span class="glyphicon glyphicon-log-in"></span> Connexion</button>
              </div> 
            </form>
        </div>  
      </div>
    </div>
  </body>
</html>
<?php include('controller/login/reset_variable_session.php'); ?>
