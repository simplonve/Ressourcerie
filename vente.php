<?php
	session_start();
	include('views/layout/header.php');
	include('views/layout/nav.php');
?>
  		<legend>
        	<h2 class="nom-ressourcerie"><?php echo $_SESSION['name_site'];?></h2>
        </legend>
        <form class="row" method="post" action="#">
			<div class="col-md-2 col-md-offset-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">1</button>
				</div>
				<div class="panel panel-info">
			    <div class="panel-heading">
					<h3 class="panel-title"><label>Type Object</label></h3>
				</div>
					<div class="panel-body">    
						<select name="vente_object" class="form-control vente-object">
							<option value="0" selected="selected"></option>
							<option value="D3E">D3E</option>
						    <optgroup label="DEA">
				       			<option value="DEA">DEA assise</option>
				                <option value="DEA">DEA couchage</option>
				                <option value="DEA">DEA rangement</option>
				                <option value="DEA">DEA plan de travail</option>
			       			</optgroup>
			       			<option value="TLC">TLC</option>
			       			<option value="Livre/Jouet">Livre/Jouet</option>
			       			<option value="Vaisselle">Vaisselle</option>
			       			<option value="Brico/Déco">Brico/Déco</option>
			                <option value="Maroquinerie">Maroquinerie</option>
			                <option value="Autres">Autres</option>
						</select>						    
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">2</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Saisie</label></h3>
					</div>
					<div class="panel-body">  
						<input type="text" class="form-control saisie" placeholder="Quantité" name="quantite_ticket"/>
						<input type="text" class="form-control saisie" placeholder="Prix unitaire" name="unit_price_ticket"/>
						<input type="text" class="form-control saisie" placeholder=". . . . , . . . kg" name="weight_ticket"/>
				    	<input type="submit" class="btn btn-success btn-lg bouton-saisie" value="Ajouter  +" />
				    	<input type="submit" class="btn btn-danger btn-lg bouton-saisie" value="Soustraire  -"/>					    
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">3</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Ticket</label></h3>
					</div>
					<div class="panel-body">    
	      				<input type="text" class="form-control total-ticket" placeholder="Total" name="total_ticket"/>
	      				<input type="submit" class="btn btn-success btn-lg bouton-ticket" value="Valider" />
	      				<input type="submit" class="btn btn-danger btn-lg bouton-ticket" value="Annuler" />	
	   				</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">4</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Encaissement</label></h3>
					</div>
					<div class="panel-body" id="divID">
						<input type="submit" class="btn btn-primary bouton-encaissement" value="Espéces"/>
						<input type="submit" class="btn btn-primary bouton-encaissement" value="Chéque"/>
						<input type="submit" class="btn btn-primary bouton-encaissement" value="Carte Bleue"/>
	   				</div>
	   			</div>
			</div>
		</form>
    </body>
</html>
<?php include('controller/login/reset_variables_session.php'); ?>