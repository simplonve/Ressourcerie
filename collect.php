<?php
	session_start();
	include('views/layout/header.php');
	include('views/layout/nav.php');
?>
  		<legend>
        	<h2 class="nom-ressourcerie"><?php echo $_SESSION['name_site'];?></h2>
        </legend>
        <?php if(isset($_SESSION['confirmation_send_collect'])){ echo $_SESSION['confirmation_send_collect'];}?>
        <?php if(isset($_SESSION['error_source'])){ echo $_SESSION['error_source'];}?>
        <?php if(isset($_SESSION['error_object'])){ echo $_SESSION['error_object'];}?>
        <?php if(isset($_SESSION['error_object_weight'])){ echo $_SESSION['error_object_weight'];}?>

        <form class="row" method="post" action="controller/collect/controller_collect.php">
			<div class="col-md-2 col-md-offset-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">1</button>
				</div>
				<div class="panel panel-info">
			    <div class="panel-heading">
					<h3 class="panel-title"><label>Type collecte</label></h3>
				</div>
					<div class="panel-body">    
						<select name="collect_source" class="form-control collect-source">
						    <option value="" selected="selected"></option> 
						    <option value="Apport volontaire">Apport volontaire</option>
			                <option value="A domicile">A domicile</option>
			                <option value="Déchetterie">Déchetterie</option>
			                <option value="Entreprise">Entreprise</option>
			                <option value="Evènement">Evènement</option>
			                <option value="Partenaire">Partenaire</option>
			                <option value="Transfert">Transfert</option>
						</select>						    
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">2</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Type objet</label></h3>
					</div>
					<div class="panel-body">  
					    <select name="object_type" class="form-control object-type">
					       <option value="" selected="selected"></option> 
			       			  <optgroup label="DEA">
				       			  <option value="DEA_1">DEA assise</option>
				                  <option value="DEA_2">DEA couchage</option>
				                  <option value="DEA_3">DEA rangement</option>
				                  <option value="DEA_4">DEA plan de travail</option>
			       			  </optgroup>
			       			  <option value="TLC">TLC</option>
			       			  <option value="D3E">D3E</option>
			       			  <option value="AE">AE</option>
			                  <option value="AUTRES">Autres</option>
					    </select>					    
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">3</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Poids objet</label></h3>
					</div>
					<div class="panel-body">    
	      				<input type="text" class="form-control poids" placeholder=". . . . , . . . kg" name="object_weight"/>	
	   				</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">4</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Validation</label></h3>
					</div>
					<div class="panel-body" id="divID">
						<input type="submit" class="btn btn-info btn-lg bouton-pese" value="Pesé!"/>
	   				</div>
	   			</div>
			</div>
		</form>
    </body>
</html>
<?php include('controller/collect/reset_variables_session.php'); ?>