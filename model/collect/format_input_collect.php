<?php 
	$object_weight = floatval(preg_replace('([,])', '.', $_POST['object_weight']));	
	preg_match('#[^0-9][0-9.]{0,}$#', $object_weight) ? $token_object_type = true : $token_object_type = false;
	$token_object_type == false ? $_SESSION['error_object_weight'] = 'Poids invalide': '';

	switch($_POST['object_type']){
		case 'DEA_1':
			$object_type = 'DEA';
			$object_sub_type = 'assises';
			break;
		case 'DEA_2':
			$object_type = 'DEA';
			$object_sub_type = 'couchage';
			break;
		case 'DEA_3':
			$object_type = 'DEA';
			$object_sub_type = 'rangement';
			break;
		case 'DEA_4':
			$object_type = 'DEA';
			$object_sub_type = 'plan de travail';
			break;
		case 'TLC':
			$object_type = 'TLC';
			$object_sub_type = '';
			break;
		case 'D3E':
			$object_type = 'D3E';
			$object_sub_type = '';
			break;
		case 'AE':
			$object_type = 'AE';
			$object_sub_type = '';
			break;
		case 'AUTRES':
			$object_type = 'AUTRES';
			$object_sub_type = '';
			break;
		default:
			$token_object_type = false;
			$_SESSION['error_object'] = 'Le type d\'objet est invalide';
			break;
	}
 ?>