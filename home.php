<?php
  session_start();
  include('views/layout/header.php');
  include('views/layout/nav.php');
?>
    <div class="row">
      <div class="container">
        <div class="col-md-3">
          <img src="images/logo-trimaranweb1.jpg" class="logo-accueil img-responsive" alt="Logo Ressourcerie">
        </div>
        <div class="col-md-9 jumbotron">
          <h1>Ressourcerie Trimaran</h1>      
          <p>Ceci est une page de présentation...</p>      
          <a href="#" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-search"></span> Rechercher</a>
        </div>
      </div>
    </div>
  </body>
</html>