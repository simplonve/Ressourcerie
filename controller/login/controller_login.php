<?php
	session_start();
// procedure connexion
	include('../../model/bdd_connection.php');
// verification login 
	// pour plus tard avec le salage SHA1
	// $password = sha1('x' . $_POST['password']);
	include('../../model/login/bdd_read_login.php');
// initialisation de la session utilisateur et des variables utilisateurs et traitement des erreurs
	if($validation_login){
		$_SESSION['user_name'] = $validation_login['user_name'];
	    $_SESSION['site'] = $validation_login['user_privilege'];
	    $validation_login['user_privilege'] == 'ollieres' ? $_SESSION['name_site'] = 'Site : Les Ollières' : '';
	    $validation_login['user_privilege'] == 'privas' ? $_SESSION['name_site'] = 'Site : Privas' : '';
	    include('../../redirects/to_home.php');
	}
	else
	{
		include('../../model/login/bdd_read_user_existence.php');
		$user_valid ? $_SESSION['error_login'] =  'Mot de passe incorrect' : $_SESSION['error_login'] = 'Nom d\'utilisateur incorrect';
		include ("../../redirects/to_index.php");
	}
?>