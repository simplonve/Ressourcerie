<?php
	session_start();
// procedure connexion
	include('../../model/bdd_connection.php');
//Contrôle des données saisies pour la collecte
	include('../../model/collect/control_input_collect.php');
// envoi de la collecte en base de données
	include('../../model/collect/send_collect_to_bdd.php');
// // redirection vers la page de saisie "collect"
	include('../../redirects/to_collect.php');
?>