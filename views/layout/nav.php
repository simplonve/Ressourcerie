<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="home.php" class="titre-nav navbar-brand">Ressourcerie Trimaran </a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li class="btn btn-nav dropdown">
                    <a href="collect.php"><span class="glyphicon glyphicon-flag"></span> - Collecte</a>
                </li>
                <li class="btn btn-nav dropdown">
                    <a href="valorization.php"><span class="glyphicon glyphicon-thumbs-up"></span> - Valorisation</a>
                </li>
                <li class="btn btn-nav dropdown">
                    <a href="vente.php"><span class="glyphicon glyphicon-shopping-cart"></span> - Boutique</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown"><span class="caret"></span> Administration </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="administration.php"><span class="glyphicon glyphicon-list-alt"></span> - Gestion utilisateurs</a>
                        </li>
                        <li>
                            <a href="bilan.php"><span class="glyphicon glyphicon-euro"></span> - Bilans</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown"><span class="caret"></span> Connexion </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="index.php"><span class="glyphicon  glyphicon-log-in"></span> S'identifier </a>
                        </li>
                        <li>
                            <a href="index.php"> Changer mot de passe </a>
                        </li>
                        <li>
                            <a href="model/logout/action_logout.php"><span class="glyphicon  glyphicon-log-out"></span> Se déconnecter </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>