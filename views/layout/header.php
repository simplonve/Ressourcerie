<!DOCTYPE html>
<html>
  	<head>
	    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
	    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/style.css" rel="stylesheet" media="screen">
	    <script src="js/jquery.min.js"></script>
    	<script src="js/bootstrap.min.js"></script>
	    <title>Ressourcerie Trimaran</title>
	    <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico" />
  	</head>
  	<body>