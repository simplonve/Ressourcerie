<?php
	session_start();
	include('views/layout/header.php');
	include('views/layout/nav.php');
?>
  		<legend>
        	<h2 class="nom-ressourcerie"><?php echo $_SESSION['name_site'];?></h2>
        </legend>
        <form class="row" method="post" action="controller/valorization/controller_valorization.php">
			<div class="col-md-2 col-md-offset-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">1</button>
				</div>
					<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Type objet</label></h3>
					</div>
					<div class="panel-body">  
					    <select name="object_type" class="form-control object-type">
					       <option value="0" selected="selected"></option> 
			       			  <optgroup label="DEA">
				       			  <option value="DEA">DEA assise</option>
				                  <option value="DEA">DEA couchage</option>
				                  <option value="DEA">DEA rangement</option>
				                  <option value="DEA">DEA plan de travail</option>
			       			  </optgroup>
			       			  <option value="TLC">TLC</option>
			       			  <option value="D3E">D3E</option>
			       			  <option value="AE">AE</option>
			                  <option value="Autres">Autres</option>
					    </select>					    
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">2</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Valorisation</label></h3>
					</div>
					<div class="panel-body">  
					    <select name="object_type" class="form-control object-type">
					       <option value="0" selected="selected"></option> 
			       			<optgroup label="Réemploi">
				       			<option value="Vente">Vente</option>
				                <option value="Interne">Interne</option>
				                <option value="Dons">Dons</option>
			       			</optgroup>
							<optgroup label="Ecologie D3E">
			       				<option value="Gros électroménager froid">Gros électroménager froid</option>
			                	<option value="Gros électroménager">Gros électroménager</option>
			                	<option value="Ecran">Ecran</option>
			                	<option value="Autres">Autres</option>
		       			  	</optgroup>
		       			  	<optgroup label="Ecomobilier DEA">
			       				<option value="Assise">Assise</option>
			                	<option value="Couchage">Couchage</option>
			                	<option value="Plan de travail/pose">Plan de travail/pose</option>
			                	<option value="Rangement">Rangement</option>
		       			  	</optgroup>
				       		<option value="Relais TLC">Relais TLC</option>
			       			<option value="Retour déchetterie">Retour déchetterie</option>
					    </select>					    
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">3</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Poids objet</label></h3>
					</div>
					<div class="panel-body">    
	      				<input type="text" class="form-control poids" placeholder=". . . . , . . . kg" name="object_weight"/>	
	   				</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="panel-number">
					<button class="btn btn-default btn-circle" type="button">4</button>
				</div>
				<div class="panel panel-info">
				    <div class="panel-heading">
						<h3 class="panel-title"><label>Validation</label></h3>
					</div>
					<div class="panel-body" id="divID">
						<input type="submit" class="btn btn-info btn-lg bouton-pese" value="Pesé!"/>
	   				</div>
	   			</div>
			</div>
		</form>
    </body>
</html>
<?php include('controller/login/reset_variables_session.php'); ?>