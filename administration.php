<?php
    session_start();
    include('views/layout/header.php');
    include('views/layout/nav.php');
?>
  		<legend>
        	<h2 class="nom-ressourcerie"><?php echo $_SESSION['name_site'];?></h2>
        </legend>
		<h3 class="titre-saisie">Saisie nouveau site :</h3>
		<div class="row">
			<form action="#" method="post">
				<div class="col-md-5">
					<input type="text" class="col-md-2 form-control new-site" placeholder="Nouveau site" name="object_weight"/>
					<input type="submit" class="col-md-3 btn btn-success btn-sm bouton-new-site" value="Créer le nouveau site" />
				</div>
			</form>
		</div>
		<h3 class="titre-saisie">Saisie nouvel utilisateur :</h3>
        <div class="row">
        	<form action="#" method="post">
				<input type="text" class="col-md-2 form-control new-user" placeholder="Utilisateur" name="user_name"/>
				<input type="text" class="col-md-2 form-control new-password" placeholder="Mot de passe" name="user_password"/>
				<div class="col-md-2">    
					<select name="user_privilege" class="form-control choix-new-site">
					    <option value="" selected="selected"></option> 
					    <option value="ollieres">Les Olliéres</option>
		                <option value="voulte">La Voulte</option>
		                <option value="privas">Privas</option>
					</select>						    
				</div>
				<div class="col-md-2">
					<input type="submit" class="btn btn-success btn-sm bouton-new-user" value="Créer le nouvel utilisateur" />
				</div>
			</form>
		</div>
		<?php
            include_once('model/bdd_connection.php');
            // On récupère tout le contenu de la table news
            $reponse = $bdd->query("SELECT * FROM `users`");
        ?>
        <h3 class="titre-saisie">Liste des utilisateurs validés :</h3>
        <div class="table table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="admin-table">Nom d'utilisateur :</th>
                        <th class="admin-table">Mot de passe :</th>
                        <th class="admin-table">Site de ressourcerie :</th>
                        <th class="admin-table"> Action:</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($data = $reponse->fetch()) {
                        echo '
                                <tr>
                                    <td>'.$data['user_name'].'</td>
                                    <td>'.$data['user_password'].'</td>
                                    <td>'.$data['user_privilege'].'</td>
                                    <td>
                                        <button class="btn btn-warning btn-sm bouton-admin">Modifier</button>
                                        <button class="btn btn-danger btn-sm bouton-admin">Supprimer</button>
                                    </td>
                                </tr>
                        ';
                    }
                ?>    
                </tbody>
            </table>
        </div>
        <?php
         
        $reponse->closeCursor(); // Termine le traitement de la requête
         
        ?>
    </body>
</html>
<?php include('controller/administration/reset_variables_session.php'); ?>